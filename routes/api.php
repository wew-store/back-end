<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

/*
|--------------------------------------------------------------------------
| Customer API Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'mobile'], function () {
    Route::resource('v1/customers', 'Api\CustomersApiController', ['as' => 'api']);
    Route::resource('v1/products', 'Api\ProductsController', ['as' => 'api']);
    Route::resource('v1/promos', 'Api\PromosController', ['as' => 'api']);
    Route::resource('v1/orders', 'Api\OrdersController', ['as' => 'api']);
    Route::resource('v1/categories', 'Api\CategoriesController', ['as' => 'api']);
    Route::resource('v1/addresses', 'Api\AddressesController', ['as' => 'api']);
});