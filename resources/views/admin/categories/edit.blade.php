@extends('admin.layouts.app', ['page' => 'category'])

@section('title', 'Edit Category')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Edit Category
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.categories.update', ['category' => $category->id]) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name', $category->name) }}"
                id="name"
            >
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control"
                name="description"
                id="description"
                required
                placeholder="Description"
            >{{ old('description', $category->description) }}</textarea>
        </div>

        <div class="form-group">
            <label class="checkbox-inline">
                <input type="hidden" name="active" value="0">
                <input type="checkbox"
                    name="active"
                    value="1"
                    {{ old('active', $category->active) == 1 ? 'checked' : '' }}
                >
                    Active
            </label>
        </div>

        <div class="form-group">
            <label for="products">Product</label>
            <select class="form-control"
                name="products[]"
                required
                multiple
                id="products"
            >
                @foreach ($products as $product)
                    <option value="{{ $product->id }}"
                        {{ in_array($product->id, old('products', $category->products)) ? 'selected' : '' }}
                    >
                        {{ $product->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Update
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.categories.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
