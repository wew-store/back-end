@extends('admin.layouts.app', ['page' => 'category'])

@section('title', 'Add New Category')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Add New Category
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.categories.store') }}">
        @csrf

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name') }}"
                id="name"
            >
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control"
                name="description"
                id="description"
                required
                placeholder="Description"
            >{{ old('description') }}</textarea>
        </div>

        <div class="form-group">
            <label class="checkbox-inline">
                <input type="hidden" name="active" value="0">
                <input type="checkbox"
                    name="active"
                    value="1"
                    {{ old('active') == 1 ? 'checked' : '' }}
                >
                    Active
            </label>
        </div>

        <div class="form-group">
            <label for="products">Product</label>
            <select class="form-control"
                name="products[]"
                required
                multiple
                id="products"
            >
                @foreach ($products as $product)
                    <option value="{{ $product->id }}"
                        {{ is_array(old('products')) && in_array($product->id, old('products')) ? 'selected' : '' }}
                    >
                        {{ $product->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Submit
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.categories.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
