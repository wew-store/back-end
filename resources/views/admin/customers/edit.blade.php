@extends('admin.layouts.app', ['page' => 'customer'])

@section('title', 'Edit Customer')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Edit Customer
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.customers.update', ['customer' => $customer->id]) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text"
                class="form-control"
                name="first_name"
                required
                placeholder="First Name"
                value="{{ old('first_name', $customer->first_name) }}"
                id="first_name"
            >
        </div>

        <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text"
                class="form-control"
                name="last_name"
                required
                placeholder="Last Name"
                value="{{ old('last_name', $customer->last_name) }}"
                id="last_name"
            >
        </div>

        <div class="form-group">
            <label for="phone">Phone</label>
            <input type="text"
                class="form-control"
                name="phone"
                required
                placeholder="Phone"
                value="{{ old('phone', $customer->phone) }}"
                id="phone"
            >
        </div>

        <div class="form-group">
            <label for="fcm_token">Fcm Token</label>
            <input type="text"
                class="form-control"
                name="fcm_token"
                required
                placeholder="Fcm Token"
                value="{{ old('fcm_token', $customer->fcm_token) }}"
                id="fcm_token"
            >
        </div>

        <div class="form-group">
            <label for="reports_count">Reports Count</label>
            <input type="number"
                class="form-control"
                name="reports_count"
                required
                placeholder="Reports Count"
                value="{{ old('reports_count', $customer->reports_count) }}"
                step="any"
                id="reports_count"
            >
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Update
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.customers.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
