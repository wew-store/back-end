@extends('admin.layouts.app', ['page' => 'order'])

@section('title', 'Edit Order')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Edit Order
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.orders.update', ['order' => $order->id]) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="status">Status</label>
            <select class="form-control"
                name="status"
                required
                id="status"
            >
                @foreach ($statusOptions as $key => $value)
                    <option value="{{ $key }}"
                        {{ old('status', $order->status) == $key ? 'selected' : '' }}
                    >
                        {{ $value }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="total">Total</label>
            <input type="number"
                class="form-control"
                name="total"
                required
                placeholder="Total"
                value="{{ old('total', $order->total) }}"
                step="any"
                id="total"
            >
        </div>

        <div class="form-group">
            <label for="total_with_discount">Total With Discount</label>
            <input type="number"
                class="form-control"
                name="total_with_discount"
                required
                placeholder="Total With Discount"
                value="{{ old('total_with_discount', $order->total_with_discount) }}"
                step="any"
                id="total_with_discount"
            >
        </div>

        <div class="form-group">
            <label for="note">Note</label>
            <textarea class="form-control"
                name="note"
                id="note"
                required
                placeholder="Note"
            >{{ old('note', $order->note) }}</textarea>
        </div>

        <div class="form-group">
            <label for="location_address">Location Address</label>
            <textarea class="form-control"
                name="location_address"
                id="location_address"
                required
                placeholder="Location Address"
            >{{ old('location_address', $order->location_address) }}</textarea>
        </div>

        <div class="form-group">
            <label for="order_details">Order Details</label>
            <textarea class="form-control"
                name="order_details"
                id="order_details"
                required
                placeholder="Order Details"
            >{{ old('order_details', $order->order_details) }}</textarea>
        </div>

        <div class="form-group">
            <label for="address-id">Address</label>
            <select class="form-control"
                name="address_id"
                required
                id="address-id"
            >
                @foreach ($addresses as $address)
                    <option value="{{ $address->id }}"
                        {{ old('address_id', $order->address_id) == $address->id ? 'selected' : '' }}
                    >
                        {{ $address->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="customer-id">Customer</label>
            <select class="form-control"
                name="customer_id"
                required
                id="customer-id"
            >
                @foreach ($customers as $customer)
                    <option value="{{ $customer->id }}"
                        {{ old('customer_id', $order->customer_id) == $customer->id ? 'selected' : '' }}
                    >
                        {{ $customer->first_name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="promo-id">Promo</label>
            <select class="form-control"
                name="promo_id"
                required
                id="promo-id"
            >
                @foreach ($promos as $promo)
                    <option value="{{ $promo->id }}"
                        {{ old('promo_id', $order->promo_id) == $promo->id ? 'selected' : '' }}
                    >
                        {{ $promo->code }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Update
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.orders.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
