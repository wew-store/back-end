@extends('admin.layouts.app', ['page' => 'address'])

@section('title', 'Add New Address')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Add New Address
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.addresses.store') }}">
        @csrf

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name') }}"
                id="name"
            >
        </div>

        <div class="form-group">
            <label for="price">Price</label>
            <input type="number"
                class="form-control"
                name="price"
                required
                placeholder="Price"
                value="{{ old('price') }}"
                step="any"
                id="price"
            >
        </div>

        <div class="form-group">
            <label for="note">Note</label>
            <textarea class="form-control"
                name="note"
                id="note"
                required
                placeholder="Note"
            >{{ old('note') }}</textarea>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Submit
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.addresses.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
