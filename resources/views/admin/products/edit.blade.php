@extends('admin.layouts.app', ['page' => 'product'])

@section('title', 'Edit Product')

@section('content')
    <div class="card-header">
        <div class="row">
            <div class="col-6 pt-2 h5">
                <i class="fa fa-tint"></i>
                Edit Product
            </div>
        </div>
    </div>

    <div class="card-body m-2">
        <form role="form" method="POST" enctype="multipart/form-data"
              action="{{ route('admin.products.update', ['product' => $product->id]) }}">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text"
                       class="form-control"
                       name="name"
                       required
                       placeholder="Name"
                       value="{{ old('name', $product->name) }}"
                       id="name"
                >
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control"
                          name="description"
                          id="description"
                          required
                          placeholder="Description"
                >{{ old('description', $product->description) }}</textarea>
            </div>

            <div class="form-group">
                <label for="cost">Cost</label>
                <input type="number"
                       class="form-control"
                       name="cost"
                       required
                       placeholder="Cost"
                       value="{{ old('cost', $product->cost) }}"
                       step="any"
                       id="cost"
                >
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                <input type="number"
                       class="form-control"
                       name="price"
                       required
                       placeholder="Price"
                       value="{{ old('price', $product->price) }}"
                       step="any"
                       id="price"
                >
            </div>

            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="hidden" name="special_offer" value="0">
                    <input type="checkbox"
                           name="special_offer"
                           value="1"
                            {{ old('special_offer', $product->special_offer) == 1 ? 'checked' : '' }}
                    >
                    Special Offer
                </label>
            </div>

            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="hidden" name="free_shipping" value="0">
                    <input type="checkbox"
                           name="free_shipping"
                           value="1"
                            {{ old('free_shipping', $product->free_shipping) == 1 ? 'checked' : '' }}
                    >
                    Free Shipping
                </label>
            </div>

            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="hidden" name="active" value="0">
                    <input type="checkbox"
                           name="active"
                           value="1"
                            {{ old('active', $product->active) == 1 ? 'checked' : '' }}
                    >
                    Active
                </label>
            </div>

            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="number"
                       class="form-control"
                       name="quantity"
                       required
                       placeholder="Quantity"
                       value="{{ old('quantity', $product->quantity) }}"
                       step="any"
                       id="quantity"
                >
            </div>

            <div class="form-group">
                <label for="sizes">Sizes</label>
                <input type="text"
                       class="form-control"
                       name="sizes"
                       required
                       placeholder="Sizes"
                       value="{{ old('sizes', $product->sizes) }}"
                       id="sizes"
                >
            </div>

            <div class="form-group">
                <label for="colors">Colors</label>
                <input type="text"
                       class="form-control"
                       name="colors"
                       required
                       placeholder="Colors"
                       value="{{ old('colors', $product->colors) }}"
                       id="colors"
                >
            </div>

            <img src="{{ $product->getFirstMediaUrl('cover') }}"
                 width="150"
                 height="auto"
                 alt="Cover image"
            >
            <div class="form-group">
                <label for="cover">Cover</label>
                <input type="file"
                       class="form-control"
                       name="cover"
                       value="{{ old('cover', $product->cover) }}"
                       id="cover"
                >
            </div>

            @foreach($product->getMedia('gallery') as $image)
                <img src="{{ $image->getUrl() }}"
                     width="50"
                     height="auto"
                     alt="Gallery image"
                >
            @endforeach

            <div class="form-group">
                <label for="gallery">Gallery</label>
                <input type="file"
                       multiple
                       class="form-control"
                       name="gallery[]"
                       value="{{ old('gallery', $product->gallery) }}"
                       id="gallery"
                >
            </div>

            <div class="form-group">
                <label for="old_price">Old Price</label>
                <input type="number"
                       class="form-control"
                       name="old_price"
                       required
                       placeholder="Old Price"
                       value="{{ old('old_price', $product->old_price) }}"
                       step="any"
                       id="old_price"
                >
            </div>

            <div class="form-group">
                <label for="categories">Categories</label>
                <select class="form-control"
                        name="categories[]"
                        required
                        multiple
                        id="categories"
                >

                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}"
                                {{ in_array($category->id, $product->categories) ? 'selected="true"' : '' }}
                        >
                            {{ $category->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-sm btn-primary">
                    Update
                </button>

                <a class="btn btn-sm btn-danger"
                   href="{{ route('admin.products.index') }}"
                >
                    Cancel
                </a>
            </div>
        </form>
    </div>
@endsection
