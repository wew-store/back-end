@extends('admin.layouts.app', ['page' => 'product'])

@section('title', 'Add New Product')

@section('content')
    <div class="card-header">
        <div class="row">
            <div class="col-6 pt-2 h5">
                <i class="fa fa-tint"></i>
                Add New Product
            </div>
        </div>
    </div>

    <div class="card-body m-2">
        <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('admin.products.store') }}">
            @csrf

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text"
                       class="form-control"
                       name="name"
                       required
                       placeholder="Name"
                       value="{{ old('name') }}"
                       id="name"
                >
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control"
                          name="description"
                          id="description"
                          required
                          placeholder="Description"
                >{{ old('description') }}</textarea>
            </div>

            <div class="form-group">
                <label for="cost">Cost</label>
                <input type="number"
                       class="form-control"
                       name="cost"
                       required
                       placeholder="Cost"
                       value="{{ old('cost') }}"
                       step="any"
                       id="cost"
                >
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                <input type="number"
                       class="form-control"
                       name="price"
                       required
                       placeholder="Price"
                       value="{{ old('price') }}"
                       step="any"
                       id="price"
                >
            </div>

            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="hidden" name="special_offer" value="0">
                    <input type="checkbox"
                           name="special_offer"
                           value="1"
                            {{ old('special_offer') == 1 ? 'checked' : '' }}
                    >
                    Special Offer
                </label>
            </div>

            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="hidden" name="free_shipping" value="0">
                    <input type="checkbox"
                           name="free_shipping"
                           value="1"
                            {{ old('free_shipping') == 1 ? 'checked' : '' }}
                    >
                    Free Shipping
                </label>
            </div>

            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="hidden" name="active" value="0">
                    <input type="checkbox"
                           name="active"
                           value="1"
                            {{ old('active') == 1 ? 'checked' : '' }}
                    >
                    Active
                </label>
            </div>

            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="number"
                       class="form-control"
                       name="quantity"
                       required
                       placeholder="Quantity"
                       value="{{ old('quantity') }}"
                       step="any"
                       id="quantity"
                >
            </div>

            <div class="form-group">
                <label for="sizes">Sizes</label>
                <input type="text"
                       class="form-control"
                       name="sizes"
                       required
                       placeholder="Sizes"
                       value="{{ old('sizes') }}"
                       id="sizes"
                >
            </div>

            <div class="form-group">
                <label for="colors">Colors</label>
                <input type="text"
                       class="form-control"
                       name="colors"
                       required
                       placeholder="Colors"
                       value="{{ old('colors') }}"
                       id="colors"
                >
            </div>

            <div class="form-group">
                <label for="cover">Cover</label>
                <input type="file"
                       class="form-control"
                       name="cover"
                       required
                       value="{{ old('cover') }}"
                       id="cover"
                >
            </div>

            <div class="form-group">
                <label for="gallery">Gallery</label>
                <input type="file"
                       multiple
                       class="form-control"
                       name="gallery[]"
                       required
                       value="{{ old('gallery') }}"
                       id="gallery"
                >
            </div>

            <div class="form-group">
                <label for="old_price">Old Price</label>
                <input type="number"
                       class="form-control"
                       name="old_price"
                       required
                       placeholder="Old Price"
                       value="{{ old('old_price') }}"
                       step="any"
                       id="old_price"
                >
            </div>

            <div class="form-group">
                <label for="orders">Categories</label>
                <select class="form-control"
                        name="categories[]"
                        required
                        multiple
                        id="orders"
                >
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}"
                                {{ is_array(old('categories')) && in_array($category->id, old('categories')) ? 'selected' : '' }}
                        >
                            {{ $category->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-sm btn-primary">
                    Submit
                </button>

                <a class="btn btn-sm btn-danger"
                   href="{{ route('admin.products.index') }}"
                >
                    Cancel
                </a>
            </div>
        </form>
    </div>
@endsection
