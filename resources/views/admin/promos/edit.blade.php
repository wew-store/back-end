@extends('admin.layouts.app', ['page' => 'promo'])

@section('title', 'Edit Promo')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Edit Promo
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.promos.update', ['promo' => $promo->id]) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="code">Code</label>
            <input type="text"
                class="form-control"
                name="code"
                required
                placeholder="Code"
                value="{{ old('code', $promo->code) }}"
                id="code"
            >
        </div>

        <div class="form-group">
            <label for="discount">Discount</label>
            <input type="number"
                class="form-control"
                name="discount"
                required
                placeholder="Discount"
                value="{{ old('discount', $promo->discount) }}"
                step="any"
                id="discount"
            >
        </div>

        <div class="form-group">
            <label for="note">Note</label>
            <textarea class="form-control"
                name="note"
                id="note"
                required
                placeholder="Note"
            >{{ old('note', $promo->note) }}</textarea>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Update
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.promos.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
