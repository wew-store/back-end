@extends('admin.layouts.app', ['page' => 'notification'])

@section('title', 'Add New Notification')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Add New Notification
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.notifications.store') }}">
        @csrf

        <div class="form-group">
            <label for="message">Message</label>
            <input type="text"
                class="form-control"
                name="message"
                required
                placeholder="Message"
                value="{{ old('message') }}"
                id="message"
            >
        </div>

        <div class="form-group">
            <label for="product_id">Product Id</label>
            <input type="text"
                class="form-control"
                name="product_id"
                required
                placeholder="Product Id"
                value="{{ old('product_id') }}"
                id="product_id"
            >
        </div>

        <div class="form-group">
            <label for="note">Note</label>
            <textarea class="form-control"
                name="note"
                id="note"
                required
                placeholder="Note"
            >{{ old('note') }}</textarea>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Submit
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.notifications.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
