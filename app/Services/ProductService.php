<?php

namespace App\Services;

use App\Product;
use Illuminate\Support\Facades\Schema;

class ProductService
{
    /**
     * Service Model
     *
     * @var Model
     */
    public $model;

    /**
     * Pagination
     *
     * @var integer
     */
    public $pagination;

    /**
     * Service Constructor
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->model = $product;
        $this->pagination = 20;
    }

    /**
     * All Model Items
     *
     * @return array
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Paginated items
     *
     * @return LengthAwarePaginator
     */
    public function paginated()
    {
        return $this->model->withAll()->paginate($this->pagination);
    }

    /**
     * Search the model
     *
     * @param $request
     * @return LengthAwarePaginator
     * @internal param mixed $payload
     */
    public function search($request)
    {
        $query = $this->model->withAll()->orderBy('created_at', 'desc');
        if ($request->has('id')) {
            $query->where($this->model->primaryKey, 'LIKE', '%' . $request->get('id') . '%');
        }

        $columns = Schema::getColumnListing('products');

        // Get spical offers only
        if ($request->has('special_offer')) {
            $query->orWhere('special_offer', '=', 1);
        }

        if ($request->has('name')) {
            $query->orWhere('name', 'LIKE', '%' . $request->get('name') . '%');
        }

        if ($request->has('description')) {
            $query->orWhere('description', 'LIKE', '%' . $request->get('description') . '%');
        }

        // Search by Category
        if ($request->has('category')) {
            $category = $request->get('category');
            $query->whereHas('categories', function ($query) use ($category) {
                $query->where('id', $category);
            });
        }

        // Custom pagination
        if ($request->has('pagination')) {
            $this->pagination = $request->get('pagination');
        }

        return $query->paginate($this->pagination)->appends([
            'search' => $request
        ]);
    }

    /**
     * Create the model item
     *
     * @param  array $payload
     * @return Model
     */
    public function create($payload)
    {
        return $this->model->create($payload);
    }

    /**
     * Find Model by ID
     *
     * @param  integer $id
     * @return Model
     */
    public function find($id)
    {
        return $this->model->withAll()->find($id);
    }

    /**
     * Model update
     *
     * @param  integer $id
     * @param  array $payload
     * @return Model
     */
    public function update($id, $payload)
    {
        return $this->find($id)->update($payload);
    }

    /**
     * Destroy the model
     *
     * @param  integer $id
     * @return bool
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }

}
