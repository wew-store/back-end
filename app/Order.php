<?php

namespace App;

use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logFillable = true;

    protected static $logOnlyDirty = true;

    /**
     * Static options for the Status
     *
     * @var array
     */
    public static $statusOptions = [
        'pending' => 'pending',
        'in_progress' => 'in progress',
        'canceled' => 'canceled',
        'replacing' => 'replacing',
        'refund' => 'refund',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'total', 'total_with_discount', 'note', 'location_address', 'order_details', 'address_id', 'customer_id', 'promo_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'status' => 'required',
            'total' => 'required|numeric',
            'total_with_discount' => 'nullable|numeric',
            'note' => 'nullable|string',
            'location_address' => 'required|string',
            'order_details' => 'nullable|string',
            'address_id' => 'required|numeric|exists:addresses,id',
            'customer_id' => 'required|numeric|exists:customers,id',
            'promo_id' => 'required|numeric|exists:promos,id',
        ];
    }

    /**
     * Returns the Status of the order
     *
     * @return string
     */
    public function getStatus()
    {
        return static::$statusOptions[$this->status];
    }

    /**
     * Get the address for the Order.
     */
    public function address()
    {
        return $this->belongsTo('App\Address');
    }

    /**
     * Get the customer for the Order.
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    /**
     * Get the promo for the Order.
     */
    public function promo()
    {
        return $this->belongsTo('App\Promo');
    }

    /**
     * Get the products for the Order.
     */
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['address', 'customer', 'promo'])->paginate(10);
    }

    public function scopeWithAll($query)
    {
        $query->with([
            'address',
            'customer',
            'promo',
            'products',
        ]);
    }
}
