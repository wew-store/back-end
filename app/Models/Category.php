<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = "categories";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'name',
		'description',
		'active',
		'created_at',
		'updated_at',
		'deleted_at',

    ];

    public static $rules = [
        // create rules
    ];

    // Category 
}
