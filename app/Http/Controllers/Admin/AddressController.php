<?php

namespace App\Http\Controllers\Admin;

use App\Address;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    /**
     * Display a list of Addresses.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = Address::getList();

        return view('admin.addresses.index', compact('addresses'));
    }

    /**
     * Show the form for creating a new Address
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.addresses.add');
    }

    /**
     * Save new Address
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Address::validationRules());

        $address = Address::create($validatedData);

        return redirect()->route('admin.addresses.index')->with([
            'type' => 'success',
            'message' => 'Address added'
        ]);
    }

    /**
     * Show the form for editing the specified Address
     *
     * @param \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        return view('admin.addresses.edit', compact('address'));
    }

    /**
     * Update the Address
     *
     * @param \App\Address $address
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Address $address)
    {
        $validatedData = request()->validate(
            Address::validationRules($address->id)
        );

        $address->update($validatedData);

        return redirect()->route('admin.addresses.index')->with([
            'type' => 'success',
            'message' => 'Address Updated'
        ]);
    }

    /**
     * Delete the Address
     *
     * @param \App\Address $address
     * @return void
     */
    public function destroy(Address $address)
    {
        if ($address->orders()->count()) {
            return redirect()->route('admin.addresses.index')->with([
                'type' => 'error',
                'message' => 'This record cannot be deleted as there are relationship dependencies.'
            ]);
        }

        $address->delete();

        return redirect()->route('admin.addresses.index')->with([
            'type' => 'success',
            'message' => 'Address deleted successfully'
        ]);
    }
}
