<?php

namespace App\Http\Controllers\Admin;

use App\Notification;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    /**
     * Display a list of Notifications.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::getList();

        return view('admin.notifications.index', compact('notifications'));
    }

    /**
     * Show the form for creating a new Notification
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.notifications.add');
    }

    /**
     * Save new Notification
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Notification::validationRules());

        $notification = Notification::create($validatedData);

        return redirect()->route('admin.notifications.index')->with([
            'type' => 'success',
            'message' => 'Notification added'
        ]);
    }

    /**
     * Show the form for editing the specified Notification
     *
     * @param \App\Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        return view('admin.notifications.edit', compact('notification'));
    }

    /**
     * Update the Notification
     *
     * @param \App\Notification $notification
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Notification $notification)
    {
        $validatedData = request()->validate(
            Notification::validationRules($notification->id)
        );

        $notification->update($validatedData);

        return redirect()->route('admin.notifications.index')->with([
            'type' => 'success',
            'message' => 'Notification Updated'
        ]);
    }

    /**
     * Delete the Notification
     *
     * @param \App\Notification $notification
     * @return void
     */
    public function destroy(Notification $notification)
    {
        $notification->delete();

        return redirect()->route('admin.notifications.index')->with([
            'type' => 'success',
            'message' => 'Notification deleted successfully'
        ]);
    }
}
