<?php

namespace App\Http\Controllers\Admin;

use App\Promo;
use App\Http\Controllers\Controller;

class PromoController extends Controller
{
    /**
     * Display a list of Promos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promos = Promo::getList();

        return view('admin.promos.index', compact('promos'));
    }

    /**
     * Show the form for creating a new Promo
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promos.add');
    }

    /**
     * Save new Promo
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Promo::validationRules());

        $promo = Promo::create($validatedData);

        return redirect()->route('admin.promos.index')->with([
            'type' => 'success',
            'message' => 'Promo added'
        ]);
    }

    /**
     * Show the form for editing the specified Promo
     *
     * @param \App\Promo $promo
     * @return \Illuminate\Http\Response
     */
    public function edit(Promo $promo)
    {
        return view('admin.promos.edit', compact('promo'));
    }

    /**
     * Update the Promo
     *
     * @param \App\Promo $promo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Promo $promo)
    {
        $validatedData = request()->validate(
            Promo::validationRules($promo->id)
        );

        $promo->update($validatedData);

        return redirect()->route('admin.promos.index')->with([
            'type' => 'success',
            'message' => 'Promo Updated'
        ]);
    }

    /**
     * Delete the Promo
     *
     * @param \App\Promo $promo
     * @return void
     */
    public function destroy(Promo $promo)
    {
        if ($promo->orders()->count()) {
            return redirect()->route('admin.promos.index')->with([
                'type' => 'error',
                'message' => 'This record cannot be deleted as there are relationship dependencies.'
            ]);
        }

        $promo->delete();

        return redirect()->route('admin.promos.index')->with([
            'type' => 'success',
            'message' => 'Promo deleted successfully'
        ]);
    }
}
