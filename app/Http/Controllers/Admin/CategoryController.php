<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a list of Categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::getList();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new Category
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();

        return view('admin.categories.add', compact('products'));
    }

    /**
     * Save new Category
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Category::validationRules());

        unset($validatedData['products']);
        $category = Category::create($validatedData);

        $category->products()->sync(request('products'));

        return redirect()->route('admin.categories.index')->with([
            'type' => 'success',
            'message' => 'Category added'
        ]);
    }

    /**
     * Show the form for editing the specified Category
     *
     * @param \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $products = Product::all();

        $category->products = $category->products->pluck('id')->toArray();

        return view('admin.categories.edit', compact('category', 'products'));
    }

    /**
     * Update the Category
     *
     * @param \App\Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Category $category)
    {
        $validatedData = request()->validate(
            Category::validationRules($category->id)
        );

        unset($validatedData['products']);
        $category->update($validatedData);

        $category->products()->sync(request('products'));

        return redirect()->route('admin.categories.index')->with([
            'type' => 'success',
            'message' => 'Category Updated'
        ]);
    }

    /**
     * Delete the Category
     *
     * @param \App\Category $category
     * @return void
     */
    public function destroy(Category $category)
    {
        if ($category->products()->count()) {
            return redirect()->route('admin.categories.index')->with([
                'type' => 'error',
                'message' => 'This record cannot be deleted as there are relationship dependencies.'
            ]);
        }

        $category->delete();

        return redirect()->route('admin.categories.index')->with([
            'type' => 'success',
            'message' => 'Category deleted successfully'
        ]);
    }
}
