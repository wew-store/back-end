<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use App\Order;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a list of Products.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::getList();

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new Product
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $orders = Order::all();
        $categories = Category::all();

        return view('admin.products.add', compact('orders', 'categories'));
    }

    /**
     * Save new Product
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Product::validationRules());

        unset($validatedData['cover'], $validatedData['gallery'], $validatedData['orders']);
        $product = Product::create($validatedData);

        $product->addMediaFromRequest('cover')->toMediaCollection('cover');
        $product->addMultipleMediaFromRequest(['gallery'])
            ->each(function ($fileAdder) {
                $fileAdder->toMediaCollection('gallery');
            });

        $product->orders()->sync(request('orders'));

        return redirect()->route('admin.products.index')->with([
            'type' => 'success',
            'message' => 'Product added'
        ]);
    }

    /**
     * Show the form for editing the specified Product
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $orders = Order::all();
        $categories = Category::all();

        $product->orders = $product->orders->pluck('id')->toArray();
        $product->categories = $product->categories->pluck('id')->toArray();

        $product->cover = $product->getMedia('cover')[0]->getUrl();
        $product->gallery = $product->getMedia('gallery');

        return view('admin.products.edit', compact('product', 'orders', 'categories'));
    }

    /**
     * Update the Product
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Product $product)
    {
        $validatedData = request()->validate(
            Product::validationRules($product->id)
        );

        unset($validatedData['cover'], $validatedData['gallery'], $validatedData['orders'], $validatedData['categories']);
        $product->update($validatedData);


        $product->addMediaFromRequest('cover')->toMediaCollection('cover');

        $product->addMultipleMediaFromRequest(['gallery'])
            ->each(function ($fileAdder) {
                $fileAdder->toMediaCollection('gallery');
            });

        $product->orders()->sync(request('orders'));
        $product->categories()->sync(request('categories'));

        return redirect()->route('admin.products.index')->with([
            'type' => 'success',
            'message' => 'Product Updated'
        ]);
    }

    /**
     * Delete the Product
     *
     * @param \App\Product $product
     * @return void
     */
    public function destroy(Product $product)
    {
        if ($product->orders()->count() || $product->categories()->count()) {
            return redirect()->route('admin.products.index')->with([
                'type' => 'error',
                'message' => 'This record cannot be deleted as there are relationship dependencies.'
            ]);
        }

        $product->delete();

        return redirect()->route('admin.products.index')->with([
            'type' => 'success',
            'message' => 'Product deleted successfully'
        ]);
    }
}
