<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Address;
use App\Customer;
use App\Promo;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a list of Orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::getList();

        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new Order
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $addresses = Address::all();
        $customers = Customer::all();
        $promos = Promo::all();

        $statusOptions = Order::$statusOptions;

        return view('admin.orders.add', compact('statusOptions', 'addresses', 'customers', 'promos'));
    }

    /**
     * Save new Order
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Order::validationRules());

        $order = Order::create($validatedData);

        return redirect()->route('admin.orders.index')->with([
            'type' => 'success',
            'message' => 'Order added'
        ]);
    }

    /**
     * Show the form for editing the specified Order
     *
     * @param \App\Order $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $addresses = Address::all();
        $customers = Customer::all();
        $promos = Promo::all();

        $statusOptions = Order::$statusOptions;

        return view('admin.orders.edit', compact('order', 'statusOptions', 'addresses', 'customers', 'promos'));
    }

    /**
     * Update the Order
     *
     * @param \App\Order $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Order $order)
    {
        $validatedData = request()->validate(
            Order::validationRules($order->id)
        );

        $order->update($validatedData);

        return redirect()->route('admin.orders.index')->with([
            'type' => 'success',
            'message' => 'Order Updated'
        ]);
    }

    /**
     * Delete the Order
     *
     * @param \App\Order $order
     * @return void
     */
    public function destroy(Order $order)
    {
        if ($order->products()->count()) {
            return redirect()->route('admin.orders.index')->with([
                'type' => 'error',
                'message' => 'This record cannot be deleted as there are relationship dependencies.'
            ]);
        }

        $order->delete();

        return redirect()->route('admin.orders.index')->with([
            'type' => 'success',
            'message' => 'Order deleted successfully'
        ]);
    }
}
