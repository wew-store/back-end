<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PromoService;

class PromosController extends Controller
{
    public function __construct(PromoService $promo_service)
    {
        $this->service = $promo_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $promos = $this->service->search($request->get('code'));
        return response()->json($promos);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $promos = $this->service->search($request->search);
        return response()->json($promos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\PromoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return response()->json($result);
        }

        return response()->json(['error' => 'Unable to create promo'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promo = $this->service->find($id);
        return response()->json($promo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\PromoRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return response()->json($result);
        }

        return response()->json(['error' => 'Unable to update promo'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return response()->json(['success' => 'Promo was deleted'], 200);
        }

        return response()->json(['error' => 'Unable to delete promo'], 500);
    }
}
