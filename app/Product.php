<?php

namespace App;

use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model implements HasMedia
{
    use SoftDeletes, LogsActivity, HasMediaTrait;

    protected static $logFillable = true;

    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'cost', 'price', 'special_offer', 'free_shipping', 'active', 'quantity', 'sizes', 'colors', 'cover', 'gallery', 'old_price'
    ];

    protected $appends = ['cover', 'gallery'];
    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'name' => 'nullable|string',
            'description' => 'nullable|string',
            'cost' => 'required|numeric',
            'price' => 'nullable|numeric',
            'special_offer' => 'nullable|boolean',
            'free_shipping' => 'nullable|boolean',
            'active' => 'nullable|boolean',
            'quantity' => 'nullable|numeric',
            'sizes' => 'nullable|string',
            'colors' => 'nullable|string',
            'cover' => 'nullable|image',
//            'gallery' => 'nullable|array',
            'old_price' => 'nullable|numeric',
            'orders' => 'array',
            'orders.*' => 'required|numeric|exists:orders,id',
        ];
    }

    /**
     * Spatie media library collections
     *
     * @return void
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('cover')->singleFile();

        $this->addMediaCollection('gallery');
    }

    /**
     * Get the orders for the Product.
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }

    /**
     * Get the categories for the Product.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['media'])->paginate(10);
    }

    public function scopeWithAll($query)
    {
        $query->with('categories', 'media');
    }

    public function getCoverAttribute($product)
    {
        // Add cover URL
        if($this->hasMedia('cover')){
            return $this->getMedia('cover')[0]->getUrl();
        }

        return '';
    }

    public function getGalleryAttribute($product)
    {
        if($this->hasMedia('gallery')) {
            $gallery = [];
            if (!empty($this->getMedia('gallery'))) {
                foreach ($this->getMedia('gallery') as $imageGallery) {
                    array_push($gallery, $imageGallery->getUrl());
                }
            }
            return $gallery;
        }
        return [];
    }
}
