## wewstore-real-2

This application is built using Laravel Factory.

### Installation
1. Setup a site in your local environment.
2. Create a database.
3. Copy and extract the .zip file in the project directory.
4. Set config options in the `.env` file.
5. Run `bash install.sh` in the terminal. (You can also run commands of the install.sh file one-by-one manually)

### Access

Open `yoursiteurl.dev/admin` to access the admin panel.
- Username: `admin`
- Password: `123456`

### Details about the app
#### Admin panel theme: [Core Ui](https://github.com/coreui/coreui-free-bootstrap-admin-template)

#### Models
1. Address
2. Category
3. Customer
4. Product
5. Notification
6. Order
7. Promo

#### Packages
1. [Laravel Activitylog](https://github.com/spatie/laravel-activitylog)
2. [Laravel Media Library](https://github.com/spatie/laravel-medialibrary)
3. [Laravel Backup](https://github.com/spatie/laravel-backup)
4. [Laravel Debugbar](https://github.com/barryvdh/laravel-debugbar)
5. [Laravel Log Enhancer](https://github.com/freshbitsweb/laravel-log-enhancer)

Made with Love by [Laravel Factory](https://laravelfactory.com/).
