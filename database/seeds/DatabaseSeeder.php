<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(AddressTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(NotificationTableSeeder::class);
        $this->call(OrderTableSeeder::class);
        $this->call(PromoTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
    }
}
