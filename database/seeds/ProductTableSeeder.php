<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Product::class, 100)->create()->each(function ($product) {
            $product->addMediaFromUrl('https://source.unsplash.com/random/200x200')
                ->toMediaCollection('cover')
            ;

            $product->addMediaFromUrl('https://source.unsplash.com/random/200x200')
                ->toMediaCollection('gallery')
            ;
        });
    }
}
