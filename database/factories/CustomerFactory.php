<?php

use Faker\Generator as Faker;

$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name(),
        'last_name' => $faker->name(),
        'phone' => $faker->phoneNumber(),
        'fcm_token' => $faker->word(),
        'reports_count' => $faker->randomDigitNotNull(),
    ];
});
