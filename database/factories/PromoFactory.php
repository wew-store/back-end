<?php

use Faker\Generator as Faker;

$factory->define(App\Promo::class, function (Faker $faker) {
    return [
        'code' => $faker->word(),
        'discount' => $faker->randomFloat(2, 0, 99),
        'note' => $faker->paragraph(),
    ];
});
