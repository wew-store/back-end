<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'description' => $faker->paragraph(),
        'cost' => $faker->randomFloat(2, 0, 99),
        'price' => $faker->randomFloat(2, 0, 99),
        'special_offer' => $faker->boolean(),
        'free_shipping' => $faker->boolean(),
        'active' => $faker->boolean(),
        'quantity' => $faker->randomDigitNotNull(),
        'sizes' => $faker->word(),
        'colors' => $faker->word(),
        'old_price' => $faker->randomFloat(2, 0, 99),
    ];
});
