<?php

use Faker\Generator as Faker;

$factory->define(App\Notification::class, function (Faker $faker) {
    return [
        'message' => $faker->word(),
        'product_id' => $faker->word(),
        'note' => $faker->paragraph(),
    ];
});
