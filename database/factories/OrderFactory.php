<?php

use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
        'status' => $faker->randomElement(['pending', 'in_progress', 'canceled', 'replacing', 'refund']),
        'total' => $faker->randomFloat(2, 0, 99),
        'total_with_discount' => $faker->randomFloat(2, 0, 99),
        'note' => $faker->paragraph(),
        'location_address' => $faker->paragraph(),
        'order_details' => $faker->paragraph(),
        'address_id' => function () {
            return factory(App\Address::class)->create()->id;
        },
        'customer_id' => function () {
            return factory(App\Customer::class)->create()->id;
        },
        'promo_id' => function () {
            return factory(App\Promo::class)->create()->id;
        },
    ];
});
