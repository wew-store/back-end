<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', ['pending', 'in_progress', 'canceled', 'replacing', 'refund'])->default('pending');
            $table->decimal('total');
            $table->decimal('total_with_discount')->nullable();
            $table->text('note')->nullable();
            $table->text('location_address');
            $table->text('order_details')->nullable();
            $table->integer('address_id')->unsigned()->index();
            $table->integer('customer_id')->unsigned()->index();
            $table->integer('promo_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('promo_id')->references('id')->on('promos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
