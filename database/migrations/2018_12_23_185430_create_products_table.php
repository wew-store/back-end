<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->decimal('cost');
            $table->decimal('price')->nullable();
            $table->boolean('special_offer')->nullable();
            $table->boolean('free_shipping')->nullable();
            $table->boolean('active')->nullable();
            $table->integer('quantity')->nullable();
            $table->string('sizes')->nullable();
            $table->string('colors')->nullable();
            $table->decimal('old_price')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
